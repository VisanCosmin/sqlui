{-# LANGUAGE NamedFieldPuns      #-}
{-# LANGUAGE OverloadedLabels    #-}
{-# LANGUAGE OverloadedLists     #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE ScopedTypeVariables #-}


import           TextEditor

import           Control.Monad                  (void)
import           Data.Vector                    (Vector)
import           Data.Word
import           Data.Text                      (Text)

import qualified GI.GObject                     as GI
import qualified GI.Gtk                         as Gtk
import           GI.Gtk.Declarative
import           GI.Gtk.Declarative.App.Simple
import           GI.Gtk.Declarative.EventSource (fromCancellation)


data Event = TextSet Text | Closed
data State = State Text

view' :: State -> AppView Gtk.Window Event
view' (State currentValue) =
  bin Gtk.Window
      [ #title := "Hello"
      , on #deleteEvent (const (True, Closed))
      , #widthRequest := 400
      , #heightRequest := 300
      ]
      
      $ container Gtk.Box 
            [ #orientation := Gtk.OrientationVertical ] 
            [ bin Gtk.ScrolledWindow [#heightRequest := 200] $ widget Gtk.Label [ #label := currentValue ]
            , BoxChild defaultBoxChildProperties { fill = True } 
                   $ bin Gtk.ScrolledWindow [#heightRequest := 200] $ (toTextSet <$> (textEditor [] TextEditorProperties))
            ]

update' :: State -> Event -> Transition State Event
update' _ (TextSet d)   = Transition (State d) (return Nothing)
update' _ Closed        = Exit

main :: IO ()
main = void $ run App
  { view         = view'
  , update       = update'
  , inputs       = []
  , initialState = State "Nothing"
  }

toTextSet :: TextEditorEvent -> Event
toTextSet (TextEditorText t) = TextSet t