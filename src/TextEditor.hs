{-# LANGUAGE NamedFieldPuns      #-}
{-# LANGUAGE OverloadedLabels    #-}
{-# LANGUAGE OverloadedLists     #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE ScopedTypeVariables #-}
module TextEditor where

import           Control.Monad                  (void)
import           Data.Vector                    (Vector)
import           Data.Word
import           Data.Text                      (Text)

import qualified GI.GObject                     as GI
import qualified GI.Gtk                         as Gtk
import           GI.Gtk.Declarative
import           GI.Gtk.Declarative.App.Simple
import           GI.Gtk.Declarative.EventSource (fromCancellation)



data TextEditorEvent = TextEditorText Text deriving (Eq,Show) 
data TextEditorProperties = TextEditorProperties deriving (Eq)

textEditor
  :: Vector (Attribute Gtk.TextView TextEditorEvent)
  -> TextEditorProperties
  -> Widget TextEditorEvent
textEditor customAttributes customParams = Widget 
    (CustomWidget
        { customWidget
        , customCreate
        , customPatch
        , customSubscribe
        , customAttributes
        , customParams
        }
    )
    where 
        customWidget = Gtk.TextView
        customCreate props = do
            textView <- Gtk.new Gtk.TextView [ ]  
            buffer <- Gtk.textViewGetBuffer textView
            return (textView , buffer)
        customPatch old new spin
            | old == new = CustomKeep
            | otherwise = CustomModify $ \textView -> Gtk.textViewGetBuffer textView
        customSubscribe params (buffer :: Gtk.TextBuffer) textView cb = do 
            h <- Gtk.on buffer #changed $ do 
                maybeText <- Gtk.getTextBufferText buffer
                let text = maybe "" id maybeText
                cb (TextEditorText text)
            return (fromCancellation (GI.signalHandlerDisconnect buffer h))
